import extract from "extract-zip";
import { XMLParser } from "fast-xml-parser";
import formidable from "formidable";
import { readFileSync, rmSync } from "fs";
import * as gdal from "gdal-async";
import { join } from "path";
import utils from "./utils";
import utils_server from "./utils_server";

export async function parseKmz(file: formidable.File) {
  if (!file.originalFilename) {
    throw Error("Không tìm thắy originalFilename");
  }
  const fileName = file.originalFilename.split(".")[0];
  const target = join(utils_server.uploadDir, fileName);
  utils.checkKmzLayerName(fileName);
  try {
    await extract(file.filepath, { dir: target });
    const xml = new XMLParser().parse(readFileSync(join(target, "doc.kml")));

    const dataset = gdal.open(
      join(target, xml.kml.Document.GroundOverlay.Icon.href)
    );
    const bbox = xml.kml.Document.GroundOverlay.LatLonBox;
    // https://github.com/perrygeo/bbox-cheatsheet/blob/master/reference.md
    // https://stackoverflow.com/questions/63874859/convert-png-to-geotiff-with-gdal
    const geotif = join(utils_server.uploadDir, fileName + ".tif");
    gdal.translate(geotif, dataset, [
      "-co",
      "COMPRESS=DEFLATE",
      "-of",
      "GTiff",
      "-a_srs",
      "EPSG:4326",
      "-a_ullr",
      bbox.west,
      bbox.north,
      bbox.east,
      bbox.south,
    ]);
    return {
      fileName,
      filePath: geotif,
    };
  } catch (e: any) {
    throw e;
  } finally {
    rmSync(target, { recursive: true, force: true });
  }
}
