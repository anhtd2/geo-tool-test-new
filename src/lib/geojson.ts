import { promises } from "fs";

import cities from "./cities";
import MAP, { GeoJsonItem } from "./map";

export async function parseData(fileName: string): Promise<GeoJsonItem> {
  const fileContent: any = await promises.readFile(fileName);
  const data = JSON.parse(fileContent);
  let featureType = getFeatureType(data.features[0]);
  let quanKey = MAP[featureType].quan;
  let tinhKey = MAP[featureType].tinh;
  let quan: string = data.features[0].properties[quanKey];
  let tinh: string = data.features[0].properties[tinhKey];
  if (!cities.data[tinh]?.includes(quan)) {
    throw Error(`Không tìm thấy dữ liệu của tỉnh ${tinh}, ${quan}`);
  }

  for (let i = 1; i < data.features.length; i += 1) {
    let feature = data.features[i];
    if (getFeatureType(feature) !== featureType) {
      throw Error(
        JSON.stringify(feature) + " không hợp lệ, feature " + featureType
      );
    } else if (feature.properties[quanKey] !== quan) {
      throw Error(JSON.stringify(feature) + " không hợp lệ, khác " + quan);
    } else if (feature.properties[tinhKey] !== tinh) {
      throw Error(JSON.stringify(feature) + " không hợp lệ, khác " + tinh);
    }
  }

  return {
    quan,
    tinh,
    featureType,
    features: data.features,
  };
}

function getFeatureType(feature: any) {
  const properties = feature.properties;
  let featureType;
  let error = "không xác định được loại feature";
  Object.entries(MAP).forEach(([mapKey, mapValue]) => {
    for (let i = 0; i < mapValue.require.length; i += 1) {
      let value = properties[mapValue.require[i]];
      if (value === null || value === undefined) {
        return;
      }
    }
    for (let i = 0; i < mapValue.attrs.length; i += 1) {
      let attr = mapValue.attrs[i];
      if (!properties.hasOwnProperty(attr)) {
        error = mapKey + " không có thuộc tính " + attr;
        return;
      } else if (
        mapValue.lengths[attr] &&
        properties[attr] &&
        properties[attr].length > mapValue.lengths[attr]
      ) {
        error = `${mapKey} có thuộc tính ${attr} có độ dài lớn hơn ${mapValue.lengths[attr]}`;
        return;
      }
    }
    featureType = mapKey;
  });
  if (!featureType) {
    throw Error(`${JSON.stringify(feature)} không hợp lệ, ${error}`);
  }
  return featureType;
}
