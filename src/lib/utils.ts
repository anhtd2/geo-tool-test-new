import cities from "./cities";

const utils = {
  degrees2meters(lon: number, lat: number) {
    var x = (lon * 20037508.34) / 180;
    var y = Math.log(Math.tan(((90 + lat) * Math.PI) / 360)) / (Math.PI / 180);
    y = (y * 20037508.34) / 180;
    return [x, y];
  },
  delay(t: number) {
    return new Promise(function (resolve) {
      setTimeout(function () {
        resolve(true);
      }, t);
    });
  },

  abbreviate(v: number, suffixList = ["", "K", "M", "G"], division = 1000) {
    let value = v;
    for (var i = 0; i < suffixList.length; i += 1) {
      if (value > division) {
        value = value / division;
        value = value < 10 ? Math.round(value * 10) / 10 : Math.floor(value);
      } else {
        break;
      }
    }
    return value + " " + suffixList[i];
  },
  formatDate: (v: string | Date) =>
    v &&
    new Date(v).toLocaleString(undefined, {
      hourCycle: "h23",
    }),
  abbreviateFileSize: (v: number) =>
    utils.abbreviate(v, ["B", "KB", "MB", "GB"], 1024),
  isUpperCase: (str: string) => str === str.toUpperCase(),
  isLowerCase: (str: string) => str === str.toLowerCase(),

  checkKmzLayerName: (fileName: string, ignore = false) => {
    const kmzRegex =
      /^KHSDD_(?<city>[A-Z\-]+)(_(?<district>[A-Z\-]+)){0,1}(_[0-9]+){1,2}$/;
    const match = fileName.match(kmzRegex);
    if (match) {
      const city = Object.keys(cities.data).find(
        (c) => cities.normalize(c) === match.groups?.city
      );
      if (city) {
        if (match.groups?.district) {
          const district = cities.data[city].find(
            (i) => cities.normalize(i) === match.groups?.district
          );
          if (district) {
            return { city, district };
          }
          if (!ignore) {
            throw Error(fileName + " không tìm thấy quận/huyện " + district);
          } else {
            return { city, district: match.groups?.district };
          }
        } else {
          return { city };
        }
      }
      if (!ignore) {
        throw Error(fileName + " không tìm thấy tỉnh " + city);
      } else {
        return { city: match.groups?.city };
      }
    }
    if (!ignore) {
      throw Error(fileName + " không hợp lệ");
    } else {
      return {};
    }
  },
};

export default utils;
