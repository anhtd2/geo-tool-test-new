import fs from "fs";
import path from "path";

import cities from "./cities";
import { geoClient } from "./http_client";
import MAP, { GeoJsonItem, MapItem } from "./map";
import utils from "./utils";
import utilsServer from "./utils_server";

export async function createLayer(
  store: string,
  mapItem: MapItem,
  city: string,
  district?: string
) {
  const workspace = process.env.GEOSERVER_WORKSPACE;

  const layer = cities.getLayerName(mapItem.layerPrefix, city, district);

  const cityColumn = utils.isLowerCase(mapItem.tinh)
    ? mapItem.tinh
    : `"${mapItem.tinh}"`;
  let selectQuery = `select * from ${process.env.PG_SCHEMA}.${mapItem.table} 
  where ${cityColumn} = '${city.replace("'", "''")}'`;

  if (district) {
    const disTrictColumn = utils.isLowerCase(mapItem.quan)
      ? mapItem.quan
      : `"${mapItem.quan}"`;
    selectQuery += ` and ${disTrictColumn} = '${district.replace("'", "''")}'`;
  }

  const xmlBody = `<featureType>
    <name>${layer}</name>
    <nativeName>${layer}</nativeName>
    <namespace>
        <name>${workspace}</name>
    </namespace>
    <title>${layer}</title>
    <keywords>
        <string>features</string>
        <string>${layer}</string>
    </keywords>
    <srs>EPSG:4326</srs>
    <projectionPolicy>FORCE_DECLARED</projectionPolicy>
    <enabled>true</enabled>
    <metadata>
        <entry key="cachingEnabled">false</entry>
        <entry key="JDBC_VIRTUAL_TABLE">
        <virtualTable>
            <name>${layer}</name>
            <sql>${selectQuery}</sql>
            <escapeSql>false</escapeSql>
        </virtualTable>
        </entry>
    </metadata>
    <store class="dataStore">
        <name>${store}</name>
    </store>
    <maxFeatures>0</maxFeatures>
    <numDecimals>0</numDecimals>
    </featureType>`;
  await geoClient.post(
    `/rest/workspaces/${workspace}/datastores/${store}/featuretypes`,
    xmlBody,
    {
      headers: { "Content-Type": "text/xml" },
    }
  );
  if (mapItem.defaultStyle) {
    const layerData: any = {
      defaultStyle: {
        name: `${workspace}:${mapItem.defaultStyle}`,
      },
    };
    if (mapItem.styles) {
      layerData.styles = {
        style: mapItem.styles.map((item) => ({ name: `${workspace}:${item}` })),
      };
    }
    await geoClient.put(`rest/layers/${workspace}:${layer}`, {
      layer: layerData,
    });
  }
  await updateCacheLayer(workspace + ":" + layer, {
    zoomStop: 21,
    maxCachedLevel: 20,
  });
}

export async function deleteLayer(
  store: string,
  mapItem: MapItem,
  city: string,
  district?: string
) {
  const workspace = process.env.GEOSERVER_WORKSPACE;
  const layer = cities.getLayerName(mapItem.layerPrefix, city, district);
  try {
    await geoClient.delete(
      `rest/workspaces/${workspace}/datastores/${store}/featuretypes/${layer}?recurse=true`
    );
  } catch (e) {
    console.log(e);
  }
}

export async function createAllLayers(store: string, mapItem: MapItem) {
  const cityNames = Object.keys(cities.data);
  for (let i = 0; i < cityNames.length; i += 1) {
    const city = cityNames[i];
    const districtNames = cities.data[city];
    try {
      await createLayer(store, mapItem, city);
    } catch (e) {
      console.log(e);
    }
    for (let j = 0; j < districtNames.length; j += 1) {
      const district = districtNames[j];
      try {
        await createLayer(store, mapItem, city, district);
      } catch (e) {
        console.log(e);
      }
    }
  }
}

export async function deleteAllLayers(store: string, mapItem: MapItem) {
  const cityNames = Object.keys(cities.data);
  for (let i = 0; i < cityNames.length; i += 1) {
    const city = cityNames[i];
    const districtNames = cities.data[city];
    await deleteLayer(store, mapItem, city);
    for (let j = 0; j < districtNames.length; j += 1) {
      const district = districtNames[j];
      await deleteLayer(store, mapItem, city, district);
    }
  }
}

// get layer with workspace prefix
export function getFullLayerName(data: GeoJsonItem) {
  const mapData = MAP[data.featureType];
  return (
    process.env.GEOSERVER_WORKSPACE +
    ":" +
    cities.getLayerName(mapData.layerPrefix, data.tinh, data.quan)
  );
}

export async function checkRunningTasks(layer: string) {
  const workspace = process.env.GEOSERVER_WORKSPACE;
  const resp = await geoClient.get(`/gwc/rest/seed/${workspace}:${layer}.json`);
  if (resp.data["long-array-array"].length) {
    throw Error(layer + " đang tạo cache");
  }
}

export async function updateCacheLayer(
  fullLayerName: string,
  options: { zoomStop: number; maxCachedLevel: number }
) {
  await geoClient.post(
    `/gwc/rest/layers/${fullLayerName}.xml`,
    `<GeoServerLayer>
  <enabled>true</enabled>
  <name>${fullLayerName}</name>
  <mimeFormats>
    <string>image/png8</string>
  </mimeFormats>
  <gridSubsets>
    <gridSubset>
      <gridSetName>EPSG:900913</gridSetName>
      <zoomStart>0</zoomStart>
      <zoomStop>${options.zoomStop}</zoomStop>
      <minCachedLevel>0</minCachedLevel>
      <maxCachedLevel>${options.maxCachedLevel}</maxCachedLevel>
    </gridSubset>
  </gridSubsets>
  <metaWidthHeight>
    <int>4</int>
    <int>4</int>
  </metaWidthHeight>
  <autoCacheStyles>true</autoCacheStyles>
 </GeoServerLayer>`,
    { headers: { "Content-type": "text/xml" } }
  );
}

export async function reseedLayer(
  layer: string,
  options?: {
    type?: "reseed" | "seed" | "truncate";
    zoomStart?: number;
    zoomStop?: number;
    bounds?: { minx: number; miny: number; maxx: number; maxy: number };
  }
) {
  try {
    let bounds;
    if (options?.bounds) {
      const minPoint = utils.degrees2meters(
        options.bounds.minx,
        options.bounds.miny
      );
      const maxPoint = utils.degrees2meters(
        options.bounds.maxx,
        options.bounds.maxy
      );
      bounds = [minPoint[0], minPoint[1], maxPoint[0], maxPoint[1]];
    }
    await geoClient.post(`/gwc/rest/seed/${encodeURIComponent(layer)}.json`, {
      seedRequest: {
        format: "image/png8",
        type: options?.type || "reseed",
        bounds: bounds ? { coords: { double: bounds } } : undefined,
        gridSetId: "EPSG:900913",
        zoomStart: options?.zoomStart || 0,
        zoomStop: options?.zoomStart || 17,
        threadCount: 2,
      },
    });
  } catch (e: any) {
    let error = e.message;
    if (e.response) {
      error = JSON.stringify(e.response.data);
    }
    throw Error(`[Reseed ${layer}] ${error}`);
  }
}

export async function createCoverageStore(
  storeName: string,
  geotifFile: string
) {
  const workspace = process.env.GEOSERVER_WORKSPACE;
  return await geoClient.put(
    `/rest/workspaces/${workspace}/coveragestores/${storeName}/external.geotiff`,
    "file://" + geotifFile,
    { headers: { "Content-Type": "text/plain" } }
  );
}

export async function deleteCoverageStore(storeName: string) {
  const workspace = process.env.GEOSERVER_WORKSPACE;
  const oldFolder = path.join(
    process.env.GEOSERVER_FOLDER as string,
    "data_dir/gwc",
    workspace + "_" + utilsServer.layerKHSDD
  );
  await checkRunningTasks(utilsServer.layerKHSDD);
  fs.renameSync(oldFolder, oldFolder + "__");
  const resp = await geoClient.get(
    `/rest/workspaces/${workspace}/coverages/${storeName}.json`
  );
  try {
    await geoClient.delete(
      `/rest/workspaces/${workspace}/coveragestores/${storeName}?recurse=true&purge=all`
    );
  } catch (e) {
    throw e;
  } finally {
    fs.renameSync(oldFolder + "__", oldFolder);
    // remove cache
    await reseedLayer(workspace + ":" + utilsServer.layerKHSDD, {
      type: "truncate",
      bounds: resp.data.coverage.nativeBoundingBox,
    });
    await reseedLayer(workspace + ":" + utilsServer.layerKHSDD, {
      type: "seed",
      bounds: resp.data.coverage.nativeBoundingBox,
    });
  }
}

export async function addToKHSDD(layerName: string) {
  const workspace = process.env.GEOSERVER_WORKSPACE;
  const layerGroup = utilsServer.layerKHSDD;
  const fullLayerName = `${workspace}:${layerName}`;
  let result;
  try {
    result = await geoClient.get(
      `/rest/workspaces/${workspace}/layergroups/${layerGroup}.json`
    );
  } catch (e: any) {
    if (e.response?.status === 404) {
      await geoClient.post(`/rest/workspaces/${workspace}/layergroups`, {
        layerGroup: {
          name: layerGroup,
          layers: { layer: [fullLayerName] },
          bounds: {
            minx: 102.09510242755299,
            miny: 8.46901049068728,
            maxx: 109.49482353452464,
            maxy: 23.43224652468563,
            crs: "EPSG:4326",
          },
        },
      });
      await updateCacheLayer(workspace + ":" + layerGroup, {
        zoomStop: 17,
        maxCachedLevel: 17,
      });
      return false;
    } else {
      throw e;
    }
  }
  let layers = result.data.layerGroup.publishables.published;
  let styles = result.data.layerGroup.styles.style;
  if (layers.name) {
    if (layers.name === fullLayerName) {
      return false;
    }
    layers = [layers];
    styles = [styles];
  } else if (layers.find((l: any) => l.name === fullLayerName)) {
    return false;
  }
  layers.push({ "@type": "layer", name: fullLayerName });
  styles.push(styles[0]);
  const oldFolder = path.join(
    process.env.GEOSERVER_FOLDER as string,
    "data_dir/gwc",
    workspace + "_" + layerGroup
  );
  await checkRunningTasks(utilsServer.layerKHSDD);
  try {
    // prevent geoserver remove cache after update layergroup
    fs.renameSync(oldFolder, oldFolder + "__");
    await geoClient.put(
      `/rest/workspaces/${workspace}/layergroups/${layerGroup}.json`,
      {
        layerGroup: {
          publishables: { published: layers },
          styles: { style: styles },
        },
      }
    );
    return true;
  } catch (e) {
    throw e;
  } finally {
    fs.renameSync(oldFolder + "__", oldFolder);
  }
}
