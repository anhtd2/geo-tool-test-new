export class Task {
  name: string;
  createdAt: Date;
  fileSize?: number;
  finishTime?: Date;
  error?: string;

  constructor(name: string) {
    this.name = name;
    this.createdAt = new Date();
  }

  finish() {
    this.finishTime = new Date();
  }
}

const DURATION = 3 * 24 * 3600 * 1000;

export class GeoJsonTask extends Task {
  deleteCount?: number;
  insertCount?: number;
  insertTotalCount?: number;
  note?: {
    featureType: string;
    city: string;
    district: string;
  };
}

export class KmzTask extends Task {
  layerName?: string;
  geotifFile?: string;
}

class TaskManager<T extends Task> {
  tasks = {} as { [key: string]: T };

  refresh() {
    const now = new Date().getTime();
    for (let taskName in this.tasks) {
      let finish = this.tasks[taskName].finishTime?.getTime();
      if (finish && now - finish > DURATION) {
        delete this.tasks[taskName];
      }
    }
  }

  addTask(task: T) {
    this.refresh();
    const name = task.name;
    if (this.tasks[name] && !this.tasks[name].finishTime) {
      return false;
    }
    this.tasks[name] = task;
    return true;
  }

  getTasks() {
    return Object.values(this.tasks).sort(
      (a, b) => b.createdAt.getTime() - a.createdAt.getTime()
    );
  }
}

export const geoJsonTaskManager = new TaskManager<GeoJsonTask>();
export const kmzTaskManager = new TaskManager<KmzTask>();
