import type { GetServerSideProps, NextPage } from "next";
import { useRouter } from "next/router";
import { getCookie } from "cookies-next";

import { authUser } from "./api/user";
import UploadFiles from "../component/UploadFiles";
import { Tab, Tabs } from "react-bootstrap";
import FileQuyetDinh from "../component/FileQuyetDinh";
import KmzStats from "../component/KmzStats";
import GeojsonStats from "../component/GeojsonStats";
import { GeoJsonTaskTable } from "../component/GeoJsonTaskTable";
import { KmzTaskTable } from "../component/KmzTaskTable";
import FileQuyetDinhStats from "../component/FileQuyetDinhStats";

const Home: NextPage = () => {
  const router = useRouter();

  return (
    <Tabs
      activeKey={(router.query.tab as string) || "geojson"}
      id="main"
      className="mb-3"
      mountOnEnter
      unmountOnExit
      onSelect={(tab) => {
        router.push(".?tab=" + tab, undefined, { shallow: true });
      }}
    >
      <Tab eventKey="geojson" title="GeoJson">
        <UploadFiles api="/api/geojson" accept=".geojson" />
        <GeoJsonTaskTable />
      </Tab>
      <Tab eventKey="kmz" title="KMZ">
        <UploadFiles api="/api/kmz" accept=".kmz" />
        <KmzTaskTable />
      </Tab>
      <Tab eventKey="file" title="File quyết định">
        <FileQuyetDinh />
      </Tab>
      <Tab eventKey="kmz_stat" title="Thống kê KMZ">
        <KmzStats />
      </Tab>
      <Tab eventKey="geojson_stat" title="Thống kê Geojson">
        <GeojsonStats />
      </Tab>
      <Tab eventKey="file_qd_stat" title="Thống kê file quyết định">
        <FileQuyetDinhStats />
      </Tab>
    </Tabs>
  );
};

export const getServerSideProps: GetServerSideProps = async (context) => {
  const key = getCookie("k", context) as string;
  if (authUser(key)) {
    return { props: {} };
  }
  return {
    redirect: {
      permanent: false,
      destination: "/login",
    },
    props: {},
  };
};

export default Home;
