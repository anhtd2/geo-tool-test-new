import {
  GetServerSideProps,
  InferGetServerSidePropsType,
  NextPage,
} from "next";
import ToolDbGis from "../../../component/tooldbgis";
import { PATH_LOCATON } from "../../../component/tooldbgis/const";
import { baseGetApi } from "../../../component/tooldbgis/baseApi";

export const getServerSideProps: GetServerSideProps = async (context) => {
  const { query } = context;

  if (PATH_LOCATON[query.name as keyof typeof PATH_LOCATON]) {
    try {
      const res = await baseGetApi<any>({
        path: `${PATH_LOCATON[query.name as keyof typeof PATH_LOCATON].path}/${
          query.id
        }`,
        baseUrl: process.env.BASE_DOMAIN_LOCATION,
      });

      return {
        props: {
          name: query.name,
          mode: "edit",
          listField:
            PATH_LOCATON[query.name as keyof typeof PATH_LOCATON].listField,
          dataField: res,
        },
      };
    } catch (err) {
      return {
        notFound: true,
      };
    }
  }
  return {
    notFound: true,
  };
};

const ToolDbGisPage = (pageProps: any) => {
  return <ToolDbGis {...pageProps} />;
};

export default ToolDbGisPage;
