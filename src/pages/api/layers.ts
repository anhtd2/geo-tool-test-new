import { NextApiRequest, NextApiResponse } from "next";

import { createAllLayers, deleteAllLayers } from "../../lib/layers";
import MAP from "../../lib/map";
import { checkAuth } from "./user";

export default async function handler(
  req: NextApiRequest,
  res: NextApiResponse
) {
  if (checkAuth(req, res)) {
    const mapItem = MAP[req.body.type || req.query.type];
    if (mapItem) {
      if (req.method === "POST") {
        await createAllLayers(req.body.store, mapItem);
        res.status(200).json({ status: 1 });
        return;
      } else if (req.method === "DELETE") {
        await deleteAllLayers(req.query.store as string, mapItem);
        res.status(200).json({ status: 1 });
        return;
      }
    }
    res.status(400).json({});
  }
}
