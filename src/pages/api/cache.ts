import os from "os";
import { NextApiRequest, NextApiResponse } from "next";
import { geoClient } from "../../lib/http_client";
import { checkRunningTasks, reseedLayer } from "../../lib/layers";
import utilsServer from "../../lib/utils_server";
import { checkAuth } from "./user";
import utils from "../../lib/utils";

async function reseedKHSSD(layerName: string) {
  const workspace = process.env.GEOSERVER_WORKSPACE;
  const resp = await geoClient.get(
    `/rest/workspaces/${workspace}/coverages/${layerName}.json`
  );
  await reseedLayer(workspace + ":" + utilsServer.layerKHSDD, {
    type: "truncate",
    bounds: resp.data.coverage.nativeBoundingBox,
  });
  // wait to finish truncation
  await utils.delay(5000);
  await reseedLayer(workspace + ":" + utilsServer.layerKHSDD, {
    type: "seed",
    bounds: resp.data.coverage.nativeBoundingBox,
  });
}

async function reseedGeojson(layerName: string) {
  const workspace = process.env.GEOSERVER_WORKSPACE;
  const fullLayerName = workspace + ":" + layerName;
  await checkRunningTasks(layerName);
  await reseedLayer(fullLayerName, { type: "reseed", zoomStop: 19 });
}

export default async function handler(
  req: NextApiRequest,
  res: NextApiResponse
) {
  if (checkAuth(req, res)) {
    const layer: string = req.body.layer;
    if (layer) {
      try {
        const resp = await geoClient.get(`/gwc/rest/seed.json`);
        if (resp.data["long-array-array"].length >= os.cpus().length) {
          throw Error(
            `Hệ thống đang có ${resp.data["long-array-array"].length} task tạo cache`
          );
        }
        if (layer.startsWith("KHSDD")) {
          await reseedKHSSD(layer);
        } else {
          await reseedGeojson(layer);
        }
        return res.status(200).json({ status: 1 });
      } catch (e: any) {
        return res.status(200).json({ error: e.message });
      }
    }
    res.status(404).send("Not found");
  }
}
