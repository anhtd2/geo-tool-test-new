import { NextApiRequest, NextApiResponse } from "next";

import { createTables } from "../../lib/map";
import { checkAuth } from "./user";

export default async function handler(
  req: NextApiRequest,
  res: NextApiResponse
) {
  if (checkAuth(req, res)) {
    if (req.method === "POST") {
      await createTables();
      res.status(200).json({ status: 1 });
      return;
    }
    res.status(404).json({});
  }
}
