import formidable from "formidable";
import { NextApiRequest, NextApiResponse } from "next";
import { geoClient } from "../../lib/http_client";
import { parseKmz } from "../../lib/kmz";
import {
  addToKHSDD,
  createCoverageStore,
  deleteCoverageStore,
} from "../../lib/layers";

import { KmzTask, kmzTaskManager } from "../../lib/tasks";
import utils_server from "../../lib/utils_server";
import utils from "../../lib/utils";
import { checkAuth } from "./user";

export const config = {
  api: {
    bodyParser: false,
  },
};

async function syncKHSDD() {
  const resp = await geoClient.get("/rest/layers.json");
  const layers = [];
  for (const layer of resp.data.layers.layer) {
    try {
      const layerName = layer.name.split(":")[1];
      utils.checkKmzLayerName(layerName);
      if (await addToKHSDD(layerName)) {
        layers.push(layerName);
      }
    } catch (e) {}
  }
  return layers;
}

async function handleFile(file: formidable.File) {
  const task = new KmzTask(file.originalFilename || "");
  if (kmzTaskManager.addTask(task)) {
    try {
      task.fileSize = file.size;
      const result = await parseKmz(file);
      await createCoverageStore(result.fileName, result.filePath);
      await addToKHSDD(result.fileName);
      task.layerName = result.fileName;
      task.geotifFile = result.filePath;
    } catch (e: any) {
      task.error = e.message;
    } finally {
      task.finish();
    }
  }
}

export default async function handler(
  req: NextApiRequest,
  res: NextApiResponse
) {
  if (checkAuth(req, res)) {
    if (req.method === "DELETE") {
      try {
        await deleteCoverageStore(req.query.layer as string);
        res.status(200).json({ status: 1 });
      } catch (e: any) {
        res.status(200).json({ status: 0, error: e.message });
      }
      return;
    } else if (req.method === "PATCH") {
      const layers = await syncKHSDD();
      res.status(200).json({ status: 1, layers });
    } else {
      await utils_server.uploadFiles(req, res, handleFile);
    }
  }
}
