import formidable from "formidable";
import { NextApiRequest, NextApiResponse } from "next";

import { removeDistrict, updateDatabase } from "../../lib/db";
import { parseData } from "../../lib/geojson";
import {
  createLayer,
  deleteLayer,
  getFullLayerName,
  reseedLayer,
} from "../../lib/layers";
import MAP from "../../lib/map";
import { GeoJsonTask, geoJsonTaskManager } from "../../lib/tasks";
import utils_server from "../../lib/utils_server";
import { checkAuth } from "./user";

export const config = {
  api: {
    bodyParser: false,
  },
};

async function handleFile(file: formidable.File) {
  const task = new GeoJsonTask(file.originalFilename || "");
  if (geoJsonTaskManager.addTask(task)) {
    try {
      task.fileSize = file.size;
      const data = await parseData(file.filepath);
      await updateDatabase(data, task);
      task.note = {
        featureType: data.featureType,
        city: data.tinh,
        district: data.quan,
      };

      await deleteLayer(
        process.env.GEOSERVER_STORE as string,
        MAP[data.featureType],
        data.tinh,
        data.quan
      );
      await createLayer(
        process.env.GEOSERVER_STORE as string,
        MAP[data.featureType],
        data.tinh,
        data.quan
      );
      await reseedLayer(getFullLayerName(data));
    } catch (e: any) {
      task.error = e.message;
    } finally {
      task.finish();
    }
  }
}

export default async function handler(
  req: NextApiRequest,
  res: NextApiResponse
) {
  if (checkAuth(req, res)) {
    if (req.method === "DELETE") {
      try {
        const count = await removeDistrict(
          req.query.featureType as string,
          req.query.city as string,
          req.query.district as string
        );
        res.status(200).json({ status: 1, count });
      } catch (e: any) {
        res.status(200).json({ status: 0, error: e.message });
      }
      return;
    } else {
      await utils_server.uploadFiles(req, res, handleFile);
    }
  }
}
