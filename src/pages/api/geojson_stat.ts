import { NextApiRequest, NextApiResponse } from "next";
import db from "../../lib/db";
import MAP from "../../lib/map";
import { checkAuth } from "./user";

export default async function handler(
  req: NextApiRequest,
  res: NextApiResponse
) {
  if (checkAuth(req, res)) {
    const type = req.query.type;
    if (typeof type === "string") {
      const item = MAP[type];
      const result = await db
        .select(item.quan, item.tinh)
        .count(item.quan, item.tinh)
        .from(item.table)
        .groupBy(item.quan, item.tinh);
      res.json({
        data: result.map((d) => ({
          city: d[item.tinh],
          district: d[item.quan],
          count: d.count,
        })),
      });
    }
  }
}
