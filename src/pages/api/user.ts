// Next.js API route support: https://nextjs.org/docs/api-routes/introduction
import { getCookie, setCookie } from 'cookies-next';
import type { NextApiRequest, NextApiResponse } from 'next'

type Data = {
  name?: string
}

export function authUser(user: string) {
  return user === process.env.AUTH_USER;
}

export function checkAuth(req: NextApiRequest, res: NextApiResponse) {
  const k = getCookie('k', { req, res }) as string;
  if (authUser(k)) {
    return true;
  } else {
    res.status(401).send('Unauthorized');
    return false;
  }
}

export default function handler(
  req: NextApiRequest,
  res: NextApiResponse<Data>
) {
  if (req.method === 'POST') {
    const key = req.body.user;
    if (authUser(key)) {
      setCookie('k', key, { req, res, maxAge: 3600 * 24 * 30 });
      res.status(200).json({ name: 'User' });
    } else {
      res.status(401).json({});
    }
  }
}
