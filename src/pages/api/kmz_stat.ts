import { NextApiRequest, NextApiResponse } from "next";
import { geoClient } from "../../lib/http_client";
import utilsServer from "../../lib/utils_server";
import { checkAuth } from "./user";

export default async function handler(
  req: NextApiRequest,
  res: NextApiResponse
) {
  if (checkAuth(req, res)) {
    const workspace = process.env.GEOSERVER_WORKSPACE;
    const result = await geoClient.get(
      `/rest/workspaces/${workspace}/layergroups/${utilsServer.layerKHSDD}.json`
    );
    let layers = result.data.layerGroup.publishables.published;
    if (layers && !layers.length) {
      layers = [layers];
    }
    res.json({
      data: layers
        .map((l: any) =>
          (l.name as string).includes(":") ? l.name.split(":")[1] : l.name
        )
        .sort(),
    });
  }
}
