import formidable from "formidable";
import {
  createReadStream,
  existsSync,
  mkdirSync,
  copyFileSync,
  statSync,
} from "fs";
import { NextApiRequest, NextApiResponse } from "next";
import mime from "mime";
import path from "path";

import utils_server from "../../lib/utils_server";
import { checkAuth } from "./user";

export const config = {
  api: {
    bodyParser: false,
    responseLimit: false,
  },
};

export const UPLOAD_DIR = path.join(utils_server.uploadDir, "files");
export const makeUrl = (fileName: string) => {
  const stat = statSync(path.join(UPLOAD_DIR, fileName));
  return (
    process.env.UPLOAD_FILE_HOST +
    "?file=" +
    encodeURIComponent(fileName) +
    "&_=" +
    stat.mtime.getTime()
  );
};

function handleFile(file: formidable.File) {
  if (!existsSync(UPLOAD_DIR)) {
    mkdirSync(UPLOAD_DIR);
  }
  const fileName = file.originalFilename || file.newFilename;
  copyFileSync(file.filepath, path.join(UPLOAD_DIR, fileName));
  return {
    name: fileName,
    size: file.size,
    createdAt: new Date(),
    url: makeUrl(fileName),
  };
}

function getFile(req: NextApiRequest, resp: NextApiResponse) {
  const { file } = req.query;
  if (typeof file === "string") {
    const filePath = path.join(UPLOAD_DIR, file);
    if (existsSync(filePath)) {
      const stat = statSync(filePath);

      resp.writeHead(200, {
        "Content-Type": mime.getType(filePath) || "application/octet-stream",
        "Content-Length": stat.size,
      });

      var readStream = createReadStream(filePath);
      // We replaced all the event handlers with a simple call to readStream.pipe()
      readStream.pipe(resp);
      return;
    }
  }
  resp.status(404).send("Not found");
}

export default async function handler(
  req: NextApiRequest,
  res: NextApiResponse
) {
  if (req.method === "GET") {
    getFile(req, res);
  } else if (checkAuth(req, res)) {
    if (req.method === "DELETE") {
      return;
    } else if (req.method === "POST") {
      await utils_server.uploadFiles(req, res, handleFile);
    }
  }
}
