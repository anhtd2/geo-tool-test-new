import { useEffect, useState } from "react";
import { Table, Form, Spinner } from "react-bootstrap";
import cities from "../lib/cities";
import { apiClient } from "../lib/http_client";
import { GeojsonLayerActions } from "./GeoJsonTaskTable";

export const featureTypes = ["Quy hoạch", "Hiện trạng", "Giao thông"];

const GeojsonStats = () => {
  const [type, setType] = useState(featureTypes[0]);
  const [data, setData] = useState<any>();

  const fetchData = async () => {
    if (data) {
      setData(null);
    }
    const resp: any = await apiClient.get("/api/geojson_stat", {
      params: { type: type },
    });
    setData(resp.data);
  };
  useEffect(() => {
    fetchData();
  }, [type]);

  const renderTable = () => {
    if (!data) {
      return (
        <div className="text-center mt-5">
          <Spinner animation="border" />
        </div>
      );
    }
    const items = data.data as any[];
    const citiesData: {
      [key: string]: {
        districts: Array<{ name: string; count?: string }>;
        name: string;
      };
    } = {};
    items.forEach((item) => {
      if (!(item.city in citiesData)) {
        citiesData[item.city] = { districts: [], name: item.city };
      }
      citiesData[item.city].districts.push({
        name: item.district,
        count: item.count,
      });
    });
    for (let city in cities.data) {
      if (city in citiesData) {
        cities.data[city]
          .filter(
            (district) =>
              !citiesData[city].districts.find((item) => item.name === district)
          )
          .forEach((district) =>
            citiesData[city].districts.push({ name: district })
          );
      } else {
        citiesData[city] = {
          name: city,
          districts: cities.data[city].map((l) => ({ name: l })),
        };
      }
    }

    const children = [] as Array<React.ReactNode>;
    let count = 1;
    for (const city of Object.keys(citiesData).sort()) {
      const districts = citiesData[city].districts.sort((a, b) =>
        a.name > b.name ? 1 : -1
      );
      if (districts.length) {
        for (let i = 0; i < districts.length; i += 1) {
          const district = districts[i];
          if (i === 0) {
            children.push(
              <tr key={city + district.name}>
                <td>{count}</td>
                <td rowSpan={districts.length}>
                  {city}
                  <GeojsonLayerActions
                    city={city}
                    featureType={type}
                    onSuccess={fetchData}
                  />
                </td>
                <td>
                  {district.name}{" "}
                  {district.count && (
                    <GeojsonLayerActions
                      city={city}
                      district={district.name}
                      featureType={type}
                      onSuccess={fetchData}
                    />
                  )}
                </td>
                <td>{district.count}</td>
              </tr>
            );
          } else {
            children.push(
              <tr key={city + district.name}>
                <td>{count}</td>
                <td>
                  {district.name}{" "}
                  {district.count && (
                    <GeojsonLayerActions
                      city={city}
                      district={district.name}
                      featureType={type}
                      onSuccess={fetchData}
                    />
                  )}
                </td>
                <td>{district.count}</td>
              </tr>
            );
          }
          count += 1;
        }
      }
    }

    return (
      <Table hover size="sm" className="mt-5">
        <thead>
          <tr>
            <th>#</th>
            <th>City</th>
            <th>District</th>
            <th>Count</th>
          </tr>
        </thead>
        <tbody>{children}</tbody>
      </Table>
    );
  };

  return (
    <div>
      <div>
        {featureTypes.map((t) => (
          <Form.Check
            type="radio"
            inline
            value={t}
            label={t}
            key={t}
            id={"type-" + t}
            checked={type === t}
            onChange={() => setType(t)}
          />
        ))}
      </div>
      {renderTable()}
    </div>
  );
};

export default GeojsonStats;
