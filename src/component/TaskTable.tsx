import { useEffect, useState } from "react";
import { Table } from "react-bootstrap";
import { apiClient } from "../lib/http_client";
import { Task } from "../lib/tasks";
import utils from "../lib/utils";
import ViewMore from "./ViewMore";

type TableColumn = {
  title: string;
  render: (item: any) => React.ReactNode;
};

export const TaskTable = ({
  columns,
  type,
}: {
  columns: Array<TableColumn>;
  type: "geojson" | "kmz";
}) => {
  const [items, setItems] = useState<any[]>();

  const getItems = async () => {
    const resp = await apiClient.get("/api/task?type=" + type);
    setItems(resp.data.data);
  };

  useEffect(() => {
    getItems();
    const intervalId = setInterval(getItems, 5000);
    return () => clearInterval(intervalId);
  }, []);

  return (
    <Table striped hover className="mt-5">
      <thead>
        <tr>
          <th>#</th>
          <th>File</th>
          <th>Size</th>
          <th>Created time</th>
          <th>Duration</th>
          {columns.map((column) => (
            <th key={column.title}>{column.title}</th>
          ))}
          <th>Error</th>
        </tr>
      </thead>
      <tbody>
        {items?.map((item: Task, index) => (
          <tr key={item.name}>
            <td>{index + 1}</td>
            <td>{item.name}</td>
            <td>{item.fileSize && utils.abbreviateFileSize(item.fileSize)}</td>
            <td>{utils.formatDate(item.createdAt)}</td>
            <td>
              {item.finishTime &&
                utils.abbreviate(
                  new Date(item.finishTime).getTime() -
                    new Date(item.createdAt).getTime(),
                  ["ms", "s"]
                )}
            </td>
            {columns.map((column) => (
              <td key={column.title}>{column.render(item)}</td>
            ))}
            <td className="text-danger">
              {item.error && <ViewMore text={item.error} />}
            </td>
          </tr>
        ))}
      </tbody>
    </Table>
  );
};
