import { useState } from "react";
import { Button } from "react-bootstrap";

const ViewMore = ({ text, maxLength = 96 }: { text: string, maxLength?: number }) => {
    const [toggle, setToggle] = useState(false);

    if (text.length > maxLength) {
        return toggle ? (
            <span className="text-break">{text} <Button onClick={() => setToggle(false)} variant="link" size="sm">&lt;</Button></span>
        ) : (
            <span>{text.slice(0, maxLength)} <Button onClick={() => setToggle(true)} variant="link" size="sm">&gt;</Button></span>
        );
    }
    return <>{text}</>;
}

export default ViewMore;