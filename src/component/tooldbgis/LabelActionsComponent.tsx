import { Input, Button, Popover, Select } from "antd";
import {
  PlusOutlined,
  UploadOutlined,
  DownloadOutlined,
  FilterOutlined,
  SearchOutlined,
} from "@ant-design/icons";
import { useEffect, useState } from "react";
import { baseGetApi, getFullListCity, getFullListDistrict } from "./baseApi";
import { PATH_LOCATON } from "./const";

interface ILabelActions {
  addData: () => void;
  uploadFile: () => void;
  exportFile: () => void;
  setCurrentPage: any;
  setListData: any;
  name: string;
  currentPage: number;
  setTotalCount: any;
  refInputFile: any;

  valueInput: string;
  setValueInput: any;
  valueCode: string;
  setValueCode: any;
  valueLevel: any;
  setValueLevel: any;
  valueIsActive: boolean;
  setValueIsActive: any;
  valueCity: any;
  setValueCity: any;
  valueDistrict: any;
  setValueDistrict: any;
}

const LabelActions = ({
  addData,
  uploadFile,
  exportFile,
  name,
  setListData,
  currentPage,
  setCurrentPage,
  setTotalCount,
  refInputFile,

  valueInput,
  setValueInput,
  valueCode,
  setValueCode,
  valueLevel,
  setValueLevel,
  valueIsActive,
  setValueIsActive,
  valueCity,
  setValueCity,
  valueDistrict,
  setValueDistrict,
}: ILabelActions) => {
  const [cityOptions, setCityOptions] = useState([]);
  const [distrcitOptions, setDistrcitOptions] = useState([]);

  const handleLevelChange = async (value: any) => {
    setValueLevel(value);
    try {
      const res = await baseGetApi<any>({
        path: PATH_LOCATON[name as keyof typeof PATH_LOCATON].path,
        baseUrl: process.env.BASE_DOMAIN_LOCATION,
        data: {
          level: value,
          page: currentPage,
          code: valueCode ? valueCode : "",
          keyword: valueInput ? valueInput : "",
          isActive: valueIsActive ? valueIsActive : "",
          city: valueCity ? valueCity : null,
          district: valueDistrict ? valueDistrict : null,
        },
      });
      setCurrentPage(currentPage);
      setTotalCount(res.totalResults);
      const data = res.results.map((item: any, index: number) => {
        return {
          ...item,
          number:
            currentPage < 2 ? index + 1 : (currentPage - 1) * 10 + index + 1,
        };
      });
      setListData(data);
    } catch (err) {
      console.log(err);
    }
  };

  const handleActiveChange = async (value: boolean) => {
    setValueIsActive(value);
    try {
      const res = await baseGetApi<any>({
        path: PATH_LOCATON[name as keyof typeof PATH_LOCATON].path,
        baseUrl: process.env.BASE_DOMAIN_LOCATION,
        data: {
          isActive: value,
          keyword: valueInput ? valueInput : "",
          code: valueCode ? valueCode : "",
          page: currentPage,
          level: valueLevel ? valueLevel : null,
          city: valueCity ? valueCity : null,
          district: valueDistrict ? valueDistrict : null,
        },
      });
      setCurrentPage(currentPage);
      setTotalCount(res.totalResults);
      const data = res.results.map((item: any, index: number) => {
        return {
          ...item,
          number:
            currentPage < 2 ? index + 1 : (currentPage - 1) * 10 + index + 1,
        };
      });
      setListData(data);
    } catch (err) {
      console.log(err);
    }
  };

  const handleCodeChange = async (value: any) => {
    setValueCode(value.target.value);
    try {
      const res = await baseGetApi<any>({
        path: PATH_LOCATON[name as keyof typeof PATH_LOCATON].path,
        baseUrl: process.env.BASE_DOMAIN_LOCATION,
        data: {
          code: value.target.value,
          keyword: valueInput ? valueInput : "",
          page: currentPage,
          isActive: valueIsActive ? valueIsActive : "",
          level: valueLevel ? valueLevel : "",
          city: valueCity ? valueCity : null,
          district: valueDistrict ? valueDistrict : null,
        },
      });
      setCurrentPage(currentPage);
      setTotalCount(res.totalResults);
      const data = res.results.map((item: any, index: number) => {
        return {
          ...item,
          number:
            currentPage < 2 ? index + 1 : (currentPage - 1) * 10 + index + 1,
        };
      });
      setListData(data);
    } catch (err) {
      console.log(err);
    }
  };

  const handleSearchChange = async (value: any) => {
    setValueInput(value.target.value);
    if (value.target.value === "") {
      try {
        const res = await baseGetApi<any>({
          path: PATH_LOCATON[name as keyof typeof PATH_LOCATON].path,
          baseUrl: process.env.BASE_DOMAIN_LOCATION,
          data: {
            keyword: value.target.value,
            code: valueCode ? valueCode : "",
            page: currentPage,
            isActive: valueIsActive ? valueIsActive : "",
            level: valueLevel ? valueLevel : null,
            city: valueCity ? valueCity : null,
            district: valueDistrict ? valueDistrict : null,
          },
        });
        setCurrentPage(currentPage);
        setTotalCount(res.totalResults);
        const data = res.results.map((item: any, index: number) => {
          return {
            ...item,
            number:
              currentPage < 2 ? index + 1 : (currentPage - 1) * 10 + index + 1,
          };
        });
        setListData(data);
      } catch (err) {
        console.log(err);
      }
    }
  };

  const handleCityChange = async (value: string) => {
    setValueCity(value);
    try {
      const res = await baseGetApi<any>({
        path: PATH_LOCATON[name as keyof typeof PATH_LOCATON].path,
        baseUrl: process.env.BASE_DOMAIN_LOCATION,
        data: {
          keyword: valueInput ? valueInput : "",
          code: valueCode ? valueCode : "",
          page: currentPage,
          isActive: valueIsActive ? valueIsActive : "",
          level: valueLevel ? valueLevel : null,
          city: value ? value : null,
          district: value && valueDistrict ? valueDistrict : null,
        },
      });
      setCurrentPage(currentPage);
      setTotalCount(res.totalResults);
      const data = res.results.map((item: any, index: number) => {
        return {
          ...item,
          number:
            currentPage < 2 ? index + 1 : (currentPage - 1) * 10 + index + 1,
        };
      });
      setListData(data);
    } catch (err) {
      console.log(err);
    }
  };

  const handleDistrictChange = async (value: any) => {
    setValueDistrict(value);
    try {
      const res = await baseGetApi<any>({
        path: PATH_LOCATON[name as keyof typeof PATH_LOCATON].path,
        baseUrl: process.env.BASE_DOMAIN_LOCATION,
        data: {
          keyword: valueInput ? valueInput : "",
          code: valueCode ? valueCode : "",
          page: currentPage,
          isActive: valueIsActive ? valueIsActive : "",
          level: valueLevel ? valueLevel : null,
          city: valueCity ? valueCity : null,
          district: value ? value : null,
        },
      });
      setCurrentPage(currentPage);
      setTotalCount(res.totalResults);
      const data = res.results.map((item: any, index: number) => {
        return {
          ...item,
          number:
            currentPage < 2 ? index + 1 : (currentPage - 1) * 10 + index + 1,
        };
      });
      setListData(data);
    } catch (err) {
      console.log(err);
    }
  };

  const searchName = async () => {
    try {
      const res = await baseGetApi<any>({
        path: PATH_LOCATON[name as keyof typeof PATH_LOCATON].path,
        baseUrl: process.env.BASE_DOMAIN_LOCATION,
        data: {
          keyword: valueInput,
          code: valueCode ? valueCode : "",
          page: currentPage,
          isActive: valueIsActive ? valueIsActive : "",
          level: valueLevel ? valueLevel : null,
          city: valueCity ? valueCity : null,
          district: valueDistrict ? valueDistrict : null,
        },
      });
      setCurrentPage(currentPage);
      setTotalCount(res.totalResults);
      const data = res.results.map((item: any, index: number) => {
        return {
          ...item,
          number:
            currentPage < 2 ? index + 1 : (currentPage - 1) * 10 + index + 1,
        };
      });
      setListData(data);
    } catch (err) {
      console.log(err);
    }
  };

  const renderLabel = () => {
    switch (name) {
      case "city":
        return "Thành phố";
      case "district":
        return "Quận huyện";
      case "ward":
        return "Phường xã";
    }
  };

  const renderLevelOption = () => {
    switch (name) {
      case "city":
        return [
          {
            value: 1,
            label: "Tỉnh",
          },
          {
            value: 2,
            label: "Thành phố trung ương",
          },
        ];
      case "district":
        return [
          {
            value: 1,
            label: "Huyện",
          },
          {
            value: 2,
            label: "Quận",
          },
          {
            value: 3,
            label: "Thị xã",
          },
          {
            value: 4,
            label: "Thành phố",
          },
        ];
      case "ward":
        return [
          {
            value: 1,
            label: "Xã",
          },
          {
            value: 2,
            label: "Phường",
          },
          {
            value: 3,
            label: "Thị xã",
          },
        ];
    }
  };

  const getCityOptions = async () => {
    const cityOptions = await getFullListCity();
    setCityOptions(cityOptions);
  };

  const getDistrictOptions = async () => {
    const districtOptions = await getFullListDistrict({ city: valueCity });
    setDistrcitOptions(districtOptions);
  };

  useEffect(() => {
    getCityOptions();
  }, []);

  useEffect(() => {
    if (valueCity && name === "ward") {
      getDistrictOptions();
    }
  }, [valueCity, name]);

  return (
    <div className="flex justify-end">
      <>
        <div className="flex w-96 items-center">
          <Input
            placeholder="Nhập Id, Tên"
            value={valueInput}
            size="large"
            onChange={handleSearchChange}
          />
        </div>
        <Button
          className="bg-cyan-500 text-white flex items-center ml-1 mr-5"
          size="large"
          onClick={searchName}
          disabled={!valueInput}
        >
          <SearchOutlined /> Tìm kiếm
        </Button>
        <div className="mr-5">
          <Popover
            className="bg-cyan-500 text-white flex items-center"
            trigger="click"
            placement="bottom"
            content={() => (
              <>
                {name === "district" && (
                  <div className="p-2">
                    <div>Lọc theo tỉnh/thành phố</div>
                    <Select
                      className="w-full"
                      showSearch
                      allowClear
                      size="large"
                      placeholder="Chọn thành phố"
                      value={valueCity}
                      options={cityOptions}
                      onChange={handleCityChange}
                      filterOption={(search: any, item: any) => {
                        return (
                          item.label
                            .toLowerCase()
                            .indexOf(search.toLowerCase()) >= 0
                        );
                      }}
                    />
                  </div>
                )}
                {name === "ward" && (
                  <>
                    <div className="p-2">
                      <div>Lọc theo tỉnh/thành phố</div>
                      <Select
                        className="w-full"
                        showSearch
                        allowClear
                        size="large"
                        placeholder="Chọn thành phố"
                        value={valueCity}
                        options={cityOptions}
                        onChange={handleCityChange}
                        onClear={() => {
                          console.log(" : olala");
                          setDistrcitOptions([]);
                          setValueDistrict(null);
                        }}
                        filterOption={(search: any, item: any) => {
                          return (
                            item.label
                              .toLowerCase()
                              .indexOf(search.toLowerCase()) >= 0
                          );
                        }}
                      />
                    </div>
                    <div className="p-2">
                      <div>Lọc theo quận/huyện</div>
                      <Select
                        className="w-full"
                        showSearch
                        allowClear
                        size="large"
                        placeholder="Chọn quận/huyện"
                        value={valueDistrict}
                        options={distrcitOptions}
                        onChange={handleDistrictChange}
                        disabled={!valueCity}
                        filterOption={(search: any, item: any) => {
                          return (
                            item.label
                              .toLowerCase()
                              .indexOf(search.toLowerCase()) >= 0
                          );
                        }}
                      />
                    </div>
                  </>
                )}
                <div className="p-2 w-96">
                  <div>{`Lọc theo mã ${renderLabel()}`}</div>
                  <Input
                    className="w-full"
                    size="large"
                    type="number"
                    placeholder={`Nhập mã ${renderLabel()}`}
                    onChange={handleCodeChange}
                    value={valueCode}
                  />
                </div>
                <div className="p-2">
                  <div>{`Lọc theo cấp ${renderLabel()}`}</div>
                  <Select
                    className="w-full"
                    size="large"
                    placeholder={`Chọn cấp ${renderLabel()}`}
                    value={valueLevel}
                    onChange={handleLevelChange}
                    options={renderLevelOption()}
                  />
                </div>
                <div className="p-2">
                  <div>Lọc theo trạng thái hoạt động</div>
                  <Select
                    className="w-full"
                    size="large"
                    placeholder="Chọn trạng thái hoạt động"
                    onChange={handleActiveChange}
                    value={valueIsActive}
                    options={[
                      {
                        value: true,
                        label: "Đang hoạt động",
                      },
                      {
                        value: false,
                        label: "Không hoạt động",
                      },
                    ]}
                  />
                </div>
              </>
            )}
          >
            <Button className="bg-cyan-500 text-white" size="large">
              <FilterOutlined /> Lọc
            </Button>
          </Popover>
        </div>
      </>
      <div className="flex">
        <Input type="file" accept=".xlsx,.xls" ref={refInputFile} />
        <Button
          onClick={() => {
            uploadFile();
          }}
          size="large"
          className="bg-cyan-500 text-white mr-4 ml-1 flex items-center"
        >
          <UploadOutlined /> Upload file
        </Button>
      </div>

      <div className="flex">
        <Button
          onClick={addData}
          size="large"
          className="flex items-center mr-3"
        >
          <PlusOutlined /> Thêm
        </Button>

        <Button onClick={exportFile} size="large" className="flex items-center">
          <DownloadOutlined />
          Export file
        </Button>
      </div>
    </div>
  );
};

export default LabelActions;
