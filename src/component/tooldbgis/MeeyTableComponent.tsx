import { Table, Pagination } from "antd";
import clsx from "clsx";

interface IMeeyTable {
  columns: any;
  data: any;
  totalResults: number;
  currentPage: number;
  onPageChange: (page: number) => void;
  isLoading?: boolean;
  className?: string;
  labelTable?: string;
}

const MeeyTable = ({
  columns,
  data,
  totalResults,
  currentPage,
  onPageChange,
  isLoading,
  className,
  labelTable,
}: IMeeyTable) => {
  return (
    <>
      <div className="mt-4 flex w-full justify-between items-center mb-4">
        <div className="font-medium text-sm">{labelTable}</div>
        <Pagination
          current={currentPage}
          showSizeChanger={false}
          total={totalResults}
          hideOnSinglePage
          onChange={(page) => {
            onPageChange(page);
          }}
        />
      </div>
      <Table
        loading={isLoading}
        columns={columns}
        dataSource={data}
        className={clsx(className)}
        pagination={false}
      />
    </>
  );
};

export default MeeyTable;
