export const convertTime = (timeParams) => {
  const dateTime = new Date(timeParams).toLocaleDateString('en-GB')
  return dateTime
}

export const getHour = (time) => {
  const date = new Date(time)
  return `${date.getHours() > 9 ? date.getHours() : `0${date.getHours()}`}:${date.getMinutes() > 9 ? date.getMinutes() : `0${date.getMinutes()}`
    }`
}
