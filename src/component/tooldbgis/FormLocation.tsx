import { Button, Form, Input, Select, Row, Col, Switch } from "antd";
import { useRouter } from "next/router";
import { useEffect, useMemo, useState } from "react";
import { getFullListCity, getFullListDistrict } from "./baseApi";

const FormLocation = ({ listField, getValuesForm, dataField, name }: any) => {
  const [formLocation] = Form.useForm();
  const [cityOptions, setCityOptions] = useState([]);
  const [valueCity, setValueCity] = useState("");
  const [districtOptions, setDistrictOptions] = useState([]);
  const router = useRouter();

  const getCityOptions = async () => {
    const cityOptions = await getFullListCity();
    setCityOptions(cityOptions);
  };

  const getDistrictOptions = async (value: string) => {
    const districtOptions = await getFullListDistrict({ city: value });
    setDistrictOptions(districtOptions);
  };

  const handleSelect = (value: string, item: any) => {
    if (item.name === "city") {
      if (value) {
        setValueCity(value);
        getDistrictOptions(value);
        formLocation.resetFields(['district'])
      } else {
        setDistrictOptions([])
        formLocation.resetFields(['district'])
      }
    }
  };

  useEffect(() => {
    getCityOptions();
  }, []);

  useEffect(() => {
    if (dataField && name === "ward") {
      getDistrictOptions(dataField.city._id);
    }
  }, [dataField, name]);

  const initialValues = useMemo(() => {
    if (dataField) {
      const init: any = {
        ...dataField,
        longitude: dataField.location?.coordinates[0],
        latitude: dataField.location?.coordinates[1],
        "vi-name": dataField.translation[0]?.name || "",
        "vi-prefix": dataField.translation[0]?.prefix || "",
        "en-prefix": dataField.translation[1]?.prefix || "",
        "en-name": dataField.translation[1]?.name || "",
      };

      if (name === "district") {
        init.city = dataField.city._id;
      } else if (name === "ward") {
        init.city = dataField.city._id;
        init.district = dataField.district._id;
      }
      return init;
    }
    return {
      isActive: true,
    };
  }, [dataField]);

  return (
    <Form
      layout="vertical"
      scrollToFirstError={{
        block: "center",
      }}
      form={formLocation}
      onFinish={(values) => {
        getValuesForm(values);
      }}
      initialValues={initialValues}
    >
      <Row gutter={16}>
        <Col span={listField.subList ? 10 : 24}>
          {listField?.mainList?.map((item: any, index: number) => {
            if (item.type === "select") {
              let optionSelect = [];

              if (item.name === "city") {
                optionSelect = cityOptions;
              } else if (item.name === "district") {
                optionSelect = districtOptions;
              } else {
                optionSelect = item.options;
              }

              return (
                <Form.Item
                  key={index}
                  label={item.label}
                  name={item.name}
                  rules={[
                    {
                      required: item.required,
                      message: "Cần nhập thông tin này",
                    },
                  ]}
                >
                  <Select
                    options={optionSelect}
                    onSelect={(value) => {
                      handleSelect(value, item);
                    }}
                    showSearch
                  />
                </Form.Item>
              );
            } else if (item.type === "switch") {
              return (
                <Form.Item
                  key={index}
                  label={item.label}
                  name={item.name}
                  valuePropName="checked"
                  rules={[
                    {
                      required: item.required,
                      message: "Cần nhập thông tin này",
                    },
                  ]}
                >
                  <Switch
                    defaultChecked
                    checked
                    checkedChildren
                    unCheckedChildren={false}
                  />
                </Form.Item>
              );
            } else if (item.hasChild) {
              return (
                <div key={index}>
                  <div className="mb-2 font-medium">{item.label}</div>
                  <Row key={index} gutter={16}>
                    {item.children.map((ch: any, i: number) => (
                      <Col key={i} span={12}>
                        <Form.Item
                          label={ch.label}
                          name={ch.name}
                          rules={[
                            {
                              message: `Giá trị cần nằm trong khoảng ${ch.min} đến ${ch.max}`,
                              validator: (_, val) => {
                                if (ch.min <= +val && +val <= ch.max) {
                                  return Promise.resolve(val);
                                }
                                return Promise.reject();
                              },
                            },
                          ]}
                        >
                          <Input type={ch.type} />
                        </Form.Item>
                      </Col>
                    ))}
                  </Row>
                </div>
              );
            } else if (item.type === "textarea") {
              return (
                <Form.Item
                  key={index}
                  label={item.label}
                  name={item.name}
                  rules={[
                    {
                      required: item.required,
                      message: "Cần nhập thông tin này",
                    },
                  ]}
                >
                  <Input.TextArea rows={3} />
                </Form.Item>
              );
            } else {
              return (
                <Form.Item
                  key={index}
                  label={item.label}
                  name={item.name}
                  rules={[
                    {
                      required: item.required,
                      message: "Cần nhập thông tin này",
                    },
                  ]}
                >
                  <Input type={item.type} />
                </Form.Item>
              );
            }
          })}
        </Col>
        {listField.subList && (
          <Col span={14}>
            {listField.subList.map((sub: any) => {
              {
                return (
                  <Row gutter={16}>
                    {sub.list.map((item: any, index: number) => {
                      if (item.type === "select") {
                        return (
                          <Col key={index} span={12}>
                            <Form.Item
                              label={item.label}
                              name={item.name}
                              rules={[
                                {
                                  required: item.required,
                                  message: "Cần nhập thông tin này",
                                },
                              ]}
                            >
                              <Select options={item.options} />
                            </Form.Item>
                          </Col>
                        );
                      } else if (item.type === "textarea") {
                        return (
                          <Col key={index} span={12}>
                            <Form.Item
                              label={item.label}
                              name={item.name}
                              rules={[
                                {
                                  required: item.required,
                                  message: "Cần nhập thông tin này",
                                },
                              ]}
                            >
                              <Input.TextArea rows={3} />
                            </Form.Item>
                          </Col>
                        );
                      } else {
                        return (
                          <Col key={index} span={12}>
                            <Form.Item
                              key={index}
                              label={item.label}
                              name={item.name}
                              rules={[
                                {
                                  required: item.required,
                                  message: "Cần nhập thông tin này",
                                },
                              ]}
                            >
                              <Input type={item.type} />
                            </Form.Item>
                          </Col>
                        );
                      }
                    })}
                  </Row>
                );
              }
            })}
          </Col>
        )}
      </Row>
      <div className="flex justify-end sticky bottom-4">
        <Button
          onClick={() => {
            router.push(`/tooldbgis/${name}`);
          }}
        >
          Hủy
        </Button>
        <Button htmlType="submit" type="primary" className="mx-2">
          Lưu
        </Button>
        {/* <Button htmlType="submit">Lưu và thêm tiếp</Button> */}
      </div>
    </Form>
  );
};

export default FormLocation;
