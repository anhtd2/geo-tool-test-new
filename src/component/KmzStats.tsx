import { useEffect, useState } from "react";
import { Table } from "react-bootstrap";
import cities from "../lib/cities";
import { apiClient } from "../lib/http_client";
import utils from "../lib/utils";
import { PreviewKmzLayer } from "./KmzTaskTable";

const KmzStats = () => {
  const [data, setData] = useState<any>();

  const fetchData = async () => {
    const resp: any = await apiClient.get("/api/kmz_stat");
    setData(resp.data);
  };
  useEffect(() => {
    fetchData();
  }, []);

  if (data) {
    const layers = data.data as string[];
    const citiesData: {
      [key: string]: {
        districts: Array<{ name: string; layer?: string }>;
        layer?: string;
        name: string;
      };
    } = {};
    layers.forEach((layer) => {
      const { city, district } = utils.checkKmzLayerName(layer, true);
      if (city) {
        if (!(city in citiesData)) {
          citiesData[city] = { districts: [], name: city };
        }
        if (district) {
          citiesData[city].districts.push({ name: district, layer });
        } else {
          citiesData[city].layer = layer;
        }
      }
    });
    for (let city in cities.data) {
      if (city in citiesData) {
        cities.data[city]
          .filter(
            (district) =>
              !citiesData[city].districts.find((item) => item.name === district)
          )
          .forEach((district) =>
            citiesData[city].districts.push({ name: district })
          );
      } else {
        citiesData[city] = {
          name: city,
          districts: cities.data[city].map((l) => ({ name: l })),
        };
      }
    }
    const children = [] as Array<React.ReactNode>;
    let count = 1;
    for (const city of Object.keys(citiesData).sort()) {
      const districts = citiesData[city].districts.sort((a, b) =>
        a.name > b.name ? 1 : -1
      );
      if (districts.length) {
        for (let i = 0; i < districts.length; i += 1) {
          if (i === 0) {
            children.push(
              <tr key={districts[i].layer}>
                <td>{count}</td>
                <td rowSpan={districts.length}>
                  <PreviewKmzLayer {...citiesData[city]} />
                </td>
                <td>
                  <PreviewKmzLayer {...districts[i]} />
                </td>
                <td>{districts[i].layer}</td>
              </tr>
            );
          } else {
            children.push(
              <tr key={districts[i].layer}>
                <td>{count}</td>
                <td>
                  <PreviewKmzLayer {...districts[i]} />
                </td>
                <td>{districts[i].layer}</td>
              </tr>
            );
          }
          count += 1;
        }
      } else {
        children.push(
          <tr key={city}>
            <td>{count}</td>
            <td>
              <PreviewKmzLayer {...citiesData[city]} />
            </td>
            <td></td>
            <td></td>
          </tr>
        );
        count += 1;
      }
    }

    return (
      <Table hover size="sm" className="mt-5">
        <thead>
          <tr>
            <th>#</th>
            <th>City</th>
            <th>District</th>
            <th>Layer</th>
          </tr>
        </thead>
        <tbody>{children}</tbody>
      </Table>
    );
  }
  return <div />;
};

export default KmzStats;
