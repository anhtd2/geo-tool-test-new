import {
  MapContainer,
  TileLayer,
  WMSTileLayer,
  WMSTileLayerProps,
} from "react-leaflet";

const Map = ({ wmsOptions }: { wmsOptions?: WMSTileLayerProps }) => {
  let center: [number, number] = [21.025153203770298, 105.833049431444];
  if (wmsOptions?.bounds) {
    const bbox = wmsOptions?.bounds as Array<Array<number>>;
    center = [(bbox[0][0] + bbox[1][0]) / 2, (bbox[0][1] + bbox[1][1]) / 2];
  }
  return (
    <MapContainer
      center={center}
      zoom={12}
      style={{ height: "100vh", width: "100%" }}
    >
      <TileLayer
        attribution='&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
        url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
      />
      {wmsOptions && <WMSTileLayer {...wmsOptions} />}
    </MapContainer>
  );
};

export default Map;
