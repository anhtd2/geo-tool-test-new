import { useState } from "react";
import { Button, Table } from "react-bootstrap";
import { apiClient } from "../lib/http_client";
import utils from "../lib/utils";
import UploadFiles from "./UploadFiles";

export const FileQuyetDinhActions = ({ fileName }: { fileName: string }) => {
  const sync = async () => {
    const resp = await apiClient.post("/api/file_sync", {
      file: fileName,
    });
    if (resp.data.error) {
      alert("Lỗi " + resp.data.error);
    } else {
      alert("Cập nhật thành công " + resp.data.count);
    }
  };
  return (
    <span>
      <Button variant="outline-success" size="sm" onClick={() => sync()}>
        Cập nhật
      </Button>
    </span>
  );
};

const FileQuyetDinh = () => {
  const [items, setItems] = useState([]);
  return (
    <>
      <UploadFiles
        api="/api/file"
        onSuccess={(data) => setItems(data.result)}
      />
      <Table striped hover className="mt-5">
        <thead>
          <tr>
            <th>#</th>
            <th>File</th>
            <th>Url</th>
            <th>Size</th>
            <th>Created time</th>
          </tr>
        </thead>
        <tbody>
          {items?.map((item: any, index) => (
            <tr key={item.name}>
              <td>{index + 1}</td>
              <td>
                {item.name} <FileQuyetDinhActions fileName={item.name} />
              </td>
              <td>
                <a href={item.url} target="_blank" rel="noreferrer">
                  {item.url}
                </a>
              </td>
              <td>{item.size && utils.abbreviateFileSize(item.size)}</td>
              <td>{utils.formatDate(item.createdAt)}</td>
            </tr>
          ))}
        </tbody>
      </Table>
    </>
  );
};

export default FileQuyetDinh;
