import { useState } from "react";
import {
  Button,
  Col,
  Container,
  Form,
  ProgressBar,
  Row,
} from "react-bootstrap";
import { apiClient } from "../lib/http_client";

const UploadFiles = ({
  api,
  accept,
  onSuccess,
}: {
  api: string;
  accept?: string;
  onSuccess?: (data: any) => void;
}) => {
  const max = parseInt(process.env.NEXT_PUBLIC_MAX_UPLOAD_FILES as string);
  const [error, setError] = useState<string>();
  const [progress, setProgress] = useState<number>(0);
  const [files, setFiles] = useState<FileList | null>(null);

  const upload = async () => {
    if (files) {
      if (files.length > max) {
        setError("Max files " + max);
      }
      const formData = new FormData();
      Array.from(files).forEach((file) => {
        formData.append("files", file);
      });

      setProgress(0);
      setError(undefined);
      try {
        const result = await apiClient.post(api, formData, {
          headers: { "Content-Type": "multipart/form-data" },
          onUploadProgress: (progressEvent: any) => {
            const { loaded, total } = progressEvent;
            const percentage = (loaded * 100) / total;
            setProgress(+percentage.toFixed(2));
          },
        });
        onSuccess && onSuccess(result.data);
      } catch (e: any) {
        setError(e.message + ": " + e?.response?.data);
      }
    }
  };

  return (
    <Container>
      <Row className="mt-5 align-items-top">
        <Col md={{ span: 4, offset: 4 }}>
          <Form.Control
            type="file"
            accept={accept}
            multiple
            onChange={(e) => setFiles((e.target as HTMLInputElement).files)}
          />
          {error && <Form.Text className="text-danger">{error}</Form.Text>}
        </Col>
        <Col md={{ span: 4 }}>
          <Button variant="primary" onClick={upload}>
            Submit
          </Button>
        </Col>
      </Row>
      <Row className="mt-3">
        <Col md={{ span: 4, offset: 4 }}>
          <ProgressBar now={progress} label={`${progress}%`} />
        </Col>
      </Row>
    </Container>
  );
};

export default UploadFiles;
